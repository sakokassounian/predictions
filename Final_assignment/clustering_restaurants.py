#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  3 02:41:36 2020

@author: sako
"""
import requests # library to handle requests
import pandas as pd # library for data analsysis
import numpy as np # library to handle data in a vectorized manner
import folium
from geopy.geocoders import Nominatim # module to convert an address into latitude and longitude values
import branca.colormap as cm
from sklearn.cluster import DBSCAN
from sklearn.preprocessing import StandardScaler
import matplotlib.cm as cm
import matplotlib.colors as colors
import matplotlib.pyplot as plt

def dbscan_df(dataframe,features,epsilon=0.3,samples=5,vis=False):
    """We the features and a dataframe."""     
    df=dataframe.copy()
    df=df[features]

    X=np.array(df)
    X = StandardScaler().fit_transform(X)
    db = DBSCAN(eps=epsilon, min_samples=samples).fit(X)
    
    if vis:
        df.plot(kind='scatter',x='lng',y='lat',c='labels',cmap='jet')
        plt.show()

    return db.labels_   



def get_lat_long(address):
    """We pass an address and the function returns the lat and long."""
    geolocator = Nominatim(user_agent="foursquare_agent")
    location = geolocator.geocode(address)

    lat = location.latitude
    lon= location.longitude
    return lat,lon


# function that extracts the category of the venue
def get_category_type(row):
    """We pass an address and the function venue catagory."""

    try:
        categories_list = row['categories']
    except:
        categories_list = row['venue.categories']
        
    if len(categories_list) == 0:
        return None
    else:
        return categories_list[0]['name']


def explore_venue_df(latitude,longitude,LIMIT,radius):    
    #Foursquare info
    CLIENT_ID = 'XQ3QLGBS5Y3AXPLCS3AAG5D42N0C2MMOICTYDRGO0M3RWO0E' # your Foursquare ID
    CLIENT_SECRET = 'KHUVD2AXXLHF0UOA34RV2OJSSUNIHRAQKZRMLZMI1G53CXLT' # your Foursquare Secret
    VERSION = '20180604'
    
    #url to send get request 
    url = 'https://api.foursquare.com/v2/venues/explore?client_id={}&client_secret={}&ll={},{}&v={}&radius={}&limit={}'.format(CLIENT_ID, CLIENT_SECRET, latitude, longitude, VERSION, radius, LIMIT)
    results = requests.get(url).json()
    items = results['response']['groups'][0]['items']

    dataframe = pd.json_normalize(items) # flatten JSON

    # filter columns
    filtered_columns = ['venue.name', 'venue.categories'] + [col for col in dataframe.columns if col.startswith('venue.location.')] + ['venue.id']
    dataframe_filtered = dataframe.loc[:, filtered_columns]

    # filter the category for each row
    dataframe_filtered['venue.categories'] = dataframe_filtered.apply(get_category_type, axis=1)
    
    # clean columns
    dataframe_filtered.columns = [col.split('.')[-1] for col in dataframe_filtered.columns]
    
    return dataframe_filtered


def get_venue_rating(venue_id_list):       
    #Foursquare info
    CLIENT_ID = 'LASR5Z0AWM0PVB0EB01CJLYQYNLCBSCI2IPXD5UHPBX1LWUN' # your Foursquare ID
    CLIENT_SECRET = 'GDD0NSZ4A05ZPNHBC0T41TS1ICORB4QGTZKL24PI5GUGJZLB' # your Foursquare Secret
    VERSION = '20180604'
    ratings_list=[]
    
    for venue_id in venue_id_list:
        url = 'https://api.foursquare.com/v2/venues/{}?client_id={}&client_secret={}&v={}'.format(venue_id, CLIENT_ID, CLIENT_SECRET, VERSION)
        results = requests.get(url).json()
        
        try:
            ratings_list.append(results['response']['venue']['rating'])
        except:
            ratings_list.append(np.nan)

    return ratings_list



#Address for locating latitude and longitude
lat_1,long_1=get_lat_long('New York, NY')
venues_df=explore_venue_df(lat_1,long_1,200,1000)
venues_df['ratings']=get_venue_rating(venues_df.id)
venues_df['labels']=dbscan_df(venues_df,features=['lat','lng','ratings'],epsilon=0.7,samples=4)

means=venues_df.groupby('labels').mean()
mapper=dict(zip(means.index, means.ratings))
venues_df['avg_rate']=venues_df['labels'].replace(mapper)



# set color scheme for the clusters
colors_array = cm.jet(np.linspace(0, 1, len(venues_df.labels.unique())))
rainbow = [colors.rgb2hex(i) for i in colors_array]

#codes
colors_code={}
for index,value in enumerate(mapper.keys()):
    colors_code[value]= rainbow[index]


    

#create map
venues_map = folium.Map(location=[lat_1, long_1], zoom_start=15) # generate map centred around Ecco

for name,lat, lon,lbl, avg_rating,rating in zip(venues_df['name'],venues_df['lat'], venues_df['lng'],venues_df['labels'],venues_df['avg_rate'],venues_df['ratings']):
    if lbl==-1: title="outsider - rate: %0.3f"%rating
    else: title="region: %s - avg_rate: %0.3f"%(lbl+1,avg_rating) 
    label = folium.Popup(name+"->"+title, parse_html=True)
    folium.CircleMarker(
        [lat, lon],
        radius=7,
        popup=label,
        color='black',
        fill=True,
        fill_color=colors_code[lbl],
        fill_opacity=1.0).add_to(venues_map)
    
    
    
       
# display map
venues_map.save("mymap.html")






